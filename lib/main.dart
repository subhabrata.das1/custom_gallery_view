// ignore_for_file: prefer_const_constructors

import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var imagefile;
  //this function is used to get the image from camera/gallery
  Future setImage({required ImageSource source}) async {
    final file;

    file = await ImagePicker().pickImage(
        source: source, maxHeight: 1000, maxWidth: 1000, imageQuality: 100);

    if (file != null) {
      File? imgfile = File(file.path);
      Uint8List imagebyte = await imgfile.readAsBytes();
      imagefile = base64Encode(imagebyte);

      setState(() {
        // uploadeImage(imagefile);
      });
    } else {
      return;
    }
  }

  // List<DropdownMenuItem<Object?>>? items;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return [
            SliverAppBar(
              elevation: 0,
              backgroundColor: Color.fromARGB(188, 255, 255, 255),
              leading: Icon(Icons.arrow_back_ios),
              title: Text(
                "Choose a picture from album"
                // style: TextStyle(color: Colors.black)
                ,
              ),
            ),
          ];
        },
        body: Container(
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // const Text(
              //   'You have pushed the button this many times:',
              // ),
              // Text(
              //   '',
              //   style: Theme.of(context).textTheme.headlineMedium,
              // ),
              // DropdownButtonFormField(
              //     items: <String>['Dog', 'Cat', 'Tiger', 'Lion']
              //         .map<DropdownMenuItem<String>>((String value) {
              //       return DropdownMenuItem<String>(
              //         value: value,
              //         child: Text(
              //           value,
              //           style: TextStyle(fontSize: 20),
              //         ),
              //       );
              //     }).toList(),
              //     onChanged: (x) {})
              Expanded(
                child: GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 200,
                            childAspectRatio: 1 / 1.5,
                            crossAxisSpacing: 2,
                            mainAxisSpacing: 2),
                    itemCount: 20,
                    itemBuilder: (BuildContext ctx, index) {
                      return Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          // border: BoxBorder.lerp(),
                          color: Color.fromARGB(169, 158, 158, 158),
                          // borderRadius: BorderRadius.circular(15),
                        ),
                        child:
                            index == 0 ? Icon(Icons.camera) : Icon(Icons.image),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () => setImage(source: ImageSource.gallery)),
    );
  }
}
